/*Shaun Young Final Project */
//var

var A = shuffle(); //shuffle the tiles
var paircount = 0; //tracking number of matches
var tilecount = 0; //tracking number of open tiles
var lasttile = null; //tracking last open tile
var opentile = null; //tracking current open tile


//Functions adding, removing  css classes
function tog_class (id, cl) {
  var elem = document.getElementById (id);
  if (elem.classList.contains (cl) === true) {
    elem.classList.remove (cl);
  } else {
    elem.classList.add (cl);
  }
}

function add_class (id, cl) {
  var elem = document.getElementById (id);
  if (elem.classList.contains (cl) !== true) {
    elem.classList.add (cl);
  }
}

function rem_class (id, cl) {
  var elem = document.getElementById (id);
  if (elem.classList.contains (cl) === true) {
    elem.classList.remove (cl);
  }
}

//Adds/removes the selected class of a selected tile

function tog_elem(i, elem) {
  document.getElementById("tile_" + i).onclick = function() {
    tog_class("tile_" + i, "tile_open");
    setTimeout(function() {
      tog_class("tile_icon_" + i, "nh-" + elem);
    }, 0);
  };
}

//This adds the selected class of a selected tile

function add_elem (i, elem) {
  document.getElementById ("tile_" + i).onclick = function() {
    window.opentile = i;
    add_class ("tile_" + i, "tile_open");
    add_class ("tile_icon_" + i, "nh-" + elem);

    if (window.tilecount == 1) {
      if (i != window.lasttile && window.A[i] == window.A[window.lasttile]) {
        //checks if the 2 symbols match and aren't the same tile
        var first = document.getElementById ("tile_" + i); //get tile id
        var second = document.getElementById ("tile_" + window.lasttile); //get tile id

        first.classList.add ("tile_closed"); //remove tiles
        second.classList.add ("tile_closed"); //remove tiles

        first.onclick = ""; //remove event handlers
        second.onclick = ""; //remove event handlers

        window.paircount++; //increment pair count
        if (window.paircount == 8) {
          //win condition
          //alert('Winner!');//show victory banner here
          add_class ("overlay_win", "overlay_win_open");
        }
      } else {
        rem_delay (window.opentile, window.lasttile); //clears the tiles with a 1 second delay(to let the player see the tile)
      }

      window.tilecount = 0; //resets the opened tile counter to 0
    } else {
      window.lasttile = i; //sets the last tile
      window.tilecount++;
    } //increment tile count
  };
}

//Function used to remove tiles

function rem_select (i) {
  rem_class ("tile_" + i, "tile_open");
  rem_class ("tile_icon_" + i, "fa-A");
  rem_class ("tile_icon_" + i, "fa-B");
  rem_class ("tile_icon_" + i, "fa-C");
  rem_class ("tile_icon_" + i, "fa-D");
}

//this function hides tiles, with a delay

function rem_delay (first, second) {
  setTimeout (function() {
    rem_select (first); //closes open tiles
    rem_select (second); //closes open tiles
  }, 1000);
}

//Shuffles the tiles

function shuffle () {
  var j;
  var t;

  var A = [
    "A",
    "A",
    "A",
    "A",
    "B",
    "B",
    "B",
    "B",
    "C",
    "C",
    "C",
    "C",
    "D",
    "D",
    "D",
    "D"
  ];

  for (i = 0; i < 16; i++) {
    j = Math.floor(Math.random() * (i + 1));
    t = A[i];
    A[i] = A[j];
    A[j] = t;
  }
  console.log(A);
  return A;
}

//Resets all tiles

function reset_tiles () {
  for (i = 0; i < 16; i++) {
    rem_select(i); //turns around tiles
    add_elem(i, A[i]); //adds events
    rem_class("tile_" + i, "tile_closed"); //Tiles back to normal opacity..
  }
}

//Resets the game

function reset () {
  window.A = shuffle(); //shuffle tiles and reset variables
  window.paircount = 0;
  window.tilecount = 0;
  window.lasttile = null;
  window.opentile = null;

  reset_tiles (); //reset tiles

  rem_class ("overlay_win", "overlay_win_open");
}


for (i = 0; i < 16; i++) {
  add_elem (i, A[i]); //adding event handlers to the tiles.
}

document.getElementById ("overlay_win").onclick = function() {
  reset ();
};
